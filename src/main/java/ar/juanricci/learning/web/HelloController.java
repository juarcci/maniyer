package ar.juanricci.learning.web;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/greeting")
public class HelloController {
    @RequestMapping(value = "/basic", method = RequestMethod.GET)
    public String sayHello() {
        return "<h1>Hello world</h1>";
    }

    @RequestMapping("/user_entry")
    public String useForm() {
        return "<form action=\"/greeting/user_greeting\" method=\"POST\">\n" +
                "  <label for=\"fname\">First name:</label><br>\n" +
                "  <input type=\"text\" id=\"fname\" name=\"fname\"><br>\n" +
                "  <label for=\"lname\">Last name:</label><br>\n" +
                "  <input type=\"text\" id=\"lname\" name=\"lname\"><br><br>\n" +
                "  <input type=\"submit\" value=\"Submit\">\n" +
                "</form> ";
    }

    @RequestMapping(value = "/user_greeting", method = RequestMethod.POST)
    public String printUserGreeting(@RequestParam("fname") String firstname, @RequestParam("lname") String lastname) {
        return "Thanks, " + firstname + " " + lastname;
    }

    @RequestMapping("/orders/{id}")
    public String getOrder(@PathVariable("id") String id) {
        return "Order ID: " + id;
    }
}
