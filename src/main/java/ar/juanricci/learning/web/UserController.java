package ar.juanricci.learning.web;

import ar.juanricci.learning.domain.Product;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    
    @RequestMapping("/{userId}")
    public String displayUser(@PathVariable int userId) {
        return "User found: " + userId;
    }

    @RequestMapping("/{userId}/invoices")
    public String displayUserInvoices(@PathVariable int userId, 
    @RequestParam(value="d", required=false) Date date) {
        return "invoice found for user: " + userId + " on the date: " + date;
    }

    @RequestMapping("/{userId}/items")
    public List<String> displayStringJson() {
        return Arrays.asList("Shoes", "laptop", "button");
    }

    @RequestMapping("/{userId}/products_as_json")
    public List<Product> displayProductsJson() {
        return Arrays.asList(new Product(1, "shoes", 4.3),
                new Product(2, "bag", 3.4),
                new Product(3, "shirt", 6.4));
    }
}
